import React from 'react';
import './App.css';
import ExtractBrowser from './features/editor/ExtractBrowser';

function App() {
  return (
    <ExtractBrowser />
  );
}

export default App;
