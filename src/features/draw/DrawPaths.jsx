import React from "react";
import * as d3 from "d3";

export default class DrawPaths extends React.Component {
  constructor(props) {
    super(props);
  }
  chart = () => {};

  componentDidMount() {
    var width = 500;
    var height = 500;

    //Create SVG element
    var svg = d3
      .select("body")
      .append("svg")
      .attr("width", width)
      .attr("height", height);
    this.drawLine(svg, 100, 100, 200, Math.PI / 3);
    this.drawLine(svg, 100, 100, 200, Math.PI);
    this.drawLine(svg, 100, 100, 200, Math.PI / 2);
    this.drawCircle(svg, 200, 200, 30);
  }

  drawCircle(svg, cx, cy, r, text) {
    svg
      .append("circle")
      .attr("class", "square")
      .attr("cx", cx)
      .attr("cy", cy)
      .attr("r", r)
      .style("fill", "red")
      .style("stroke", "#222");
    svg
      .append("g")
      .on("click", function (d) {
        d3.select(this).style("fill", "#fff");
      })
      .append("text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .text("AAAA");
  }

  drawLine(svg, x, y, len, arc) {
    let arrowLen = 10;
    let x2 = x + Math.cos(arc) * len;
    let y2 = y + Math.sin(arc) * len;
    svg
      .append("line")
      .attr("x1", x)
      .attr("y1", y)
      .attr("x2", x2)
      .attr("y2", y2)
      .attr("stroke", "black");
    svg
      .append("line")
      .attr("x1", x2)
      .attr("y1", y2)
      .attr("x2", x2 + Math.cos(arc - (Math.PI * 11) / 12) * arrowLen)
      .attr("y2", y2 + Math.sin(arc - (Math.PI * 11) / 12) * arrowLen)
      .attr("stroke", "black");
    return svg
      .append("line")
      .attr("x1", x2)
      .attr("y1", y2)
      .attr("x2", x2 + Math.cos(arc + (Math.PI * 11) / 12) * arrowLen)
      .attr("y2", y2 + Math.sin(arc + (Math.PI * 11) / 12) * arrowLen)
      .attr("stroke", "black");
  }

  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="-91.5 -141.5 183 395.5"
        width="183"
        height="395.5"
      >
        <g class="layer relationships">
          <g class="related-pair">
            <g class="relationship" transform="translate(0,0) rotate(90)">
              <path
                class="relationship"
                d="M 65 4 L 103 4 L 103 16 L 135 0 L 103 -16 L 103 -4 L 65 -4 Z"
              ></path>

              <text>sssdddddddddddddddddddddddddd</text>
            </g>
          </g>
        </g>
        <g class="layer nodes">
          <circle
            class="node node-id-0"
            r="50"
            fill="rgb(255, 255, 255)"
            stroke="rgb(0, 0, 0)"
            stroke-width="8px"
            cx="0"
            cy="0"
          ></circle>
          <circle
            class="node node-id-1"
            r="50"
            fill="rgb(255, 255, 255)"
            stroke="rgb(0, 0, 0)"
            stroke-width="8px"
            cx="0"
            cy="200"
          ></circle>
        </g>
        <g class="layer properties">
          <g
            class="speech-bubble node-speech-bubble"
            transform="translate(0,-50) "
          >
            <path
              class="speech-bubble-outline"
              transform="scale(1,-1) "
              d="M 0 0 L -10 20 L -80 20 A 10 10 0 0 0 -90 30 L -90 80 A 10 10 0 0 0 -80 90 L 80 90 A 10 10 0 0 0 90 80 L 90 30 A 10 10 0 0 0 80 20 L 10 20 Z"
              fill="rgb(255, 255, 255)"
              stroke="rgb(0, 0, 0)"
              stroke-width="3px"
            ></path>
            <text
              class="speech-bubble-content property-key"
              x="14"
              y="-55"
              alignment-baseline="central"
              text-anchor="end"
              font-size="50px"
              font-family="'Gill Sans', 'Gill Sans MT', Calibri, sans-serif"
            >
              foo:&nbsp;
            </text>
            <text
              class="speech-bubble-content property-value"
              x="14"
              y="-55"
              alignment-baseline="central"
              font-size="50px"
              font-family="'Gill Sans', 'Gill Sans MT', Calibri, sans-serif"
            >
              bar
            </text>
          </g>
        </g>
        <g class="layer overlay"></g>
      </svg>
    );
  }
}
