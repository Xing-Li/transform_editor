import React from "react";
import * as d3 from "d3";
interface Props{
   
}

interface State{
}
export default class DrawSvg extends React.Component<Props,State> {
  svg:any;
  constructor(props:Props) {
    super(props);
  }

  componentDidMount() {
    this.svg = this.initSVG();
  }

  initSVG() {
    let svg = d3
      .select("#svgContainer")
      .append("svg:svg")
      .attr("class", "graphdiagram")
      .attr("id", "svgElement")
      .attr("width", "100%")
      .attr("height", "100%")
      // .call(d3Zoom.zoom().on("zoom", function () {
      //    svg.attr("transform", event.transform)
      // }))
      .on("dblclick.zoom", null)
      .append("g");
    return svg;
  }

  render() {
    return <div></div>;
  }
}
