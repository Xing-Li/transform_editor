import React from "react";
import * as d3 from "d3";

export default class Transform extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="svgContainer">
        <svg
          className="graphdiagram"
          id="svgElement"
          width="100%"
          height="100%"
        >
          <g>
            <line
              x1="635"
              y1="264.5"
              x2="645"
              y2="264.5"
              style={{
                stroke: "rgb(153, 153, 153)",
                strokeOpacity: 0.5,
                strokeWidth: "1px",
              }}
            ></line>
            <line
              x1="640"
              y1="259.5"
              x2="640"
              y2="269.5"
              style={{
                stroke: "rgb(153, 153, 153)",
                strokeOpacity: 0.5,
                strokeWidth: "1px",
              }}
            ></line>
          </g>
        </svg>
      </div>
    );
  }
}
